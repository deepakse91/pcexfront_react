import React, { Component } from 'react';
import { TopHeader } from './components/globel/topHeader';
import TopNavbar from './components/globel/navbar';
import Footer from './components/globel/footer';
import HomePage from './components/homePage';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";
import SignUp from './pages/signup';
import SignIn from './pages/signin';
import demoComponent from './pages/demo';


class App extends Component {
  render() {
    return (
      <Router>
        <div>
          <TopHeader marqueeData={marqueeData} />
          <TopNavbar />
          <Switch>
            <Route exact path="/" component={HomePage} />
            <Route exact path="/signup/:name" component={SignUp} />
            <Route exact path="/signin" component={SignIn} />
            <Route exact path="/demo" component={demoComponent} />
          </Switch>
          {/* <HomePage /> */}
          <Footer footerServices={footerServices} footerAbout={footerAbout} footerSupport={footerSupport} />
        </div>
      </Router>
    )
  }
}
export default App;

const marqueeData = [
  'Market closes at 11:30 AM GMT',
  'Market closes at 11:30 AM GMT',
  'Market closes at 11:30 AM GMT',
  'Market closes at 11:30 AM GMT',
  'Market closes at 11:30 AM GMT'
];

const footerServices = [
  'Trading',
  'Become a Franchise',
  'CIP'
];

const footerAbout = [
  'About Us',
  'Terms of Use',
  'Privacy Policy',
  'KYC/AML',
  'Announcements'
];

const footerSupport = [
  'Support Ticket',
  'Knowledge Centre',
  'Fees',
  'Security',
  'Contact Us'
];

