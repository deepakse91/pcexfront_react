import React from 'react';
import './index.css';
import { Button } from 'reactstrap';
import { Link } from 'react-router-dom'

const GradientButton = (props) => {
    return (
        <Link to={props.routerLink}>
            <span className="custom-btn"><span>{props.name}</span></span>
        </Link>
    )
}

export default GradientButton
