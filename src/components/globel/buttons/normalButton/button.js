import React from 'react';

const DynamicButton = (props) => {

    const changeName = (name) => {
        props.increment(name);
    }

    return (
        <button
            type={props.type}
            className={props.className}
            id={props.id}
            disabled={props.disabled}
            onClick={() => changeName(props.name)}>
            {props.name}{props.count}
        </button>
    );
};

DynamicButton.defaultProps = {
    className: "btn btn-primary",
    name: "Button",
    disabled: false
};

export default DynamicButton