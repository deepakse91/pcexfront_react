import React, { useState } from 'react';
import { Container, Col, Row } from 'reactstrap';
import { topHeaderStyle } from '../../../helpers/styles/commonStyels';
import './index.css';
import DynamicButton from '../buttons/normalButton/button';

const Footer = (props) => {

    const [state, setState] = useState({
        buttonVar: [
            { name: 'first', count: 0, type: 'button', className: 'btn btn-success btn-sm' },
            { name: 'second', count: 0, type: 'button', className: 'btn btn-warning btn-md ml-2' },
            { name: 'third', count: 0, type: 'button', className: 'btn btn-danger btn-lg ml-2' }
        ]
    });

    function increment(name) {
        let cName = state.buttonVar.map(e => {
            if (e.name === name) {
                e.count++;
            }
            return e;
        });
        setState({ buttonVar: cName });
    }

    return (
        <div style={topHeaderStyle} >
            <Container>
                <Row>
                    <Col sm="6" md="3" lg="3">
                        <h6 className="h6-head">Services</h6>
                        <ul>
                            {
                                props.footerServices.map((item, position) => {
                                    return (
                                        <li key={'a' + position}>
                                            <a>{item}</a>
                                        </li>
                                    )
                                })
                            }
                        </ul>
                        <br />
                        <div className="row">
                            <div className="col-12 col-md-6 col-lg-5">
                                <h6 className="h6-head we-accept">
                                    <span className="v-middle">We Accept</span>
                                </h6>
                            </div>
                            <div className="col-12 col-md-6 col-lg-7">
                                <img src={process.env.PUBLIC_URL + "/assets/images/icon-mm.png"} alt="" />
                            </div>
                        </div>
                    </Col>
                    <Col sm="6" md="3" lg="3">
                        <h6 className="h6-head">About</h6>
                        <ul>
                            {
                                props.footerAbout.map((item, position) => {
                                    return (
                                        <li key={'b' + position}>
                                            <a>{item}</a>
                                        </li>
                                    )
                                })
                            }
                        </ul>
                    </Col>
                    <Col sm="6" md="3" lg="3">
                        <h6 className="h6-head">Support</h6>
                        <ul>
                            {
                                props.footerSupport.map((item, position) => {
                                    return (
                                        <li key={'c' + position}>
                                            <a>{item}</a>
                                        </li>
                                    )
                                })
                            }
                        </ul>
                    </Col>
                    <Col sm="6" md="3" lg="3">
                        <h6 className="h6-head">Contact Us</h6>
                        <ul>
                            <li>
                                <span>Harju maakond, Kuusalu vald, Pudisoo küla, Männimäe, 74626</span>
                            </li>
                            <li>
                                <a>info@pcex.io</a>
                            </li>
                            <li className="mt-3">
                                <a className="" href="https://www.facebook.com/pcexchange.io/" target="_blank">
                                    <img src={process.env.PUBLIC_URL + "assets/images/facebook.png"} width="32" height="32" alt="facebook" />
                                </a>
                                <a className="pl-3" href="https://www.instagram.com/pcex_io/?hl=en" target="_blank">
                                    <img src={process.env.PUBLIC_URL + "assets/images/instagram.png"} width="32" height="32" alt="instagram" />
                                </a>
                                <a className="pl-3" href="https://www.linkedin.com/company/pcex/" target="_blank">
                                    <img src={process.env.PUBLIC_URL + "assets/images/linkedin.png"} width="32" height="32" alt="linkedin" />
                                </a>
                                <a className="pl-3" href="https://twitter.com/PCEX_IO" target="_blank">
                                    <img src={process.env.PUBLIC_URL + "assets/images/twitter.png"} width="32" height="32" alt="twitter" />
                                </a>
                            </li>
                        </ul>
                    </Col>
                </Row>

                {
                    state.buttonVar.map((item, position) => {
                        return (
                            <DynamicButton
                                key={position}
                                name={item.name}
                                count={item.count}
                                type={item.type}
                                className={item.className}
                                increment={(name) => increment(name)} />
                        )
                    })
                }


            </Container>
        </div>
    )
}

export default Footer