import React, { useState } from 'react';
import {
    Collapse,
    Navbar,
    NavbarToggler,
    NavbarBrand,
    Nav,
    NavItem,
    NavLink,
    UncontrolledDropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem,
    NavbarText,
    Container
} from 'reactstrap';
import './index.css';
import { Link } from 'react-router-dom';

const TopNavbar = () => {

    const [isOpen, setIsOpen] = useState(false);

    const toggle = () => setIsOpen(!isOpen);

    const [isSubmenu, setIsSubmenu] = useState(false);

    // const handleOpen = () => {
    //     setIsSubmenu(!isSubmenu);
    // }

    // const handleClose = () => {
    //     setIsSubmenu({ isSubmenu: false });
    // }

    return (
        <Container>
            <Navbar color="light" light expand="md">
                <span className="navbar-brand">
                    <Link to="/">
                        <img src={process.env.PUBLIC_URL + "/assets/images/log.png"} alt="" />
                    </Link>
                </span>
                <NavbarToggler onClick={toggle} />
                <Collapse isOpen={isOpen} navbar>
                    <Nav className="mr-auto" navbar className="ml-auto">
                        <NavItem>
                            <NavLink href="">Market</NavLink>
                        </NavItem>
                        <NavItem>
                            <NavLink href="">Support</NavLink>
                        </NavItem>
                        <UncontrolledDropdown nav inNavbar onMouseEnter={() => setIsSubmenu(true)} onMouseLeave={() => setIsSubmenu(false)} isOpen={isSubmenu}>
                            <DropdownToggle nav caret>
                                Partner With Us
                            </DropdownToggle>
                            <DropdownMenu right className="language-item">
                                <DropdownItem>
                                    Franchise
                                </DropdownItem>
                                <DropdownItem>
                                    CIP
                                </DropdownItem>
                            </DropdownMenu>
                        </UncontrolledDropdown>
                        <NavItem>
                            <NavLink href="">Join Us</NavLink>
                        </NavItem>
                        <UncontrolledDropdown nav inNavbar>
                            <DropdownToggle nav>
                                EN
                            </DropdownToggle>
                            <DropdownMenu right>
                                <DropdownItem>Afrikaans</DropdownItem>
                                <DropdownItem>Albanian</DropdownItem>
                                <DropdownItem>Amharic</DropdownItem>
                                <DropdownItem>Arabic</DropdownItem>
                                <DropdownItem>Armenian</DropdownItem>
                                <DropdownItem>Azerbaijani</DropdownItem>
                                <DropdownItem>Bashkir</DropdownItem>
                                <DropdownItem>Basque</DropdownItem>
                                <DropdownItem>Belarusian</DropdownItem>
                                <DropdownItem>Bengali</DropdownItem>
                                <DropdownItem>Bosnian</DropdownItem>
                            </DropdownMenu>
                        </UncontrolledDropdown>
                    </Nav>
                </Collapse>
            </Navbar>
        </Container>
    )
}

export default TopNavbar;