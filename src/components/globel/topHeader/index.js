import React from 'react';
import { Row, Col } from 'reactstrap';
import { topHeaderStyle } from '../../../helpers/styles/commonStyels';

export const TopHeader = (props) => {
    return (
        <div style={topHeaderStyle}>
            <marquee>
                <Row>
                    {
                        props.marqueeData.map((item, position) => {
                            return (
                                <Col key={position}>
                                    <small>{item}</small>
                                </Col>
                            )
                        })
                    }
                </Row>
            </marquee>
        </div >
    )
}

