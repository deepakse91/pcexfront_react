import React from 'react';
import GradientButton from '../../globel/buttons/gradientButton';
import { Container } from 'reactstrap';
import './index.css';
import { h1Style } from '../../../helpers/styles/commonStyels';
import SvgComponent from './svgContainer';

function BannerContainer() {
    return (
        <div className="slider-full">
            <div className="inn-slider">
                <div className="slider-text">
                    <h1 style={h1Style}>Buy, Sell, or Exchange <br /> Cryptocurrencies within minutes!</h1>
                    <div className="hr"></div>
                    <div className="cta-btn">
                        <GradientButton name={'SIGN UP'} routerLink={'/signup/ram'} />
                        <GradientButton name={'LOG IN'} routerLink={'/signin'} />
                        <GradientButton name={'DEMO'} routerLink={'/demo'} />
                    </div>
                </div>

                <div className="slider-img">
                    <SvgComponent />
                </div>
            </div>
        </div >
    )
}

export default BannerContainer
