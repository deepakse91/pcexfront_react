import React from 'react'
import BannerContainer from './bannerContainer'
import TopNavbar from '../globel/navbar'

const HomePage = () => {
    return (
        <React.Fragment>
            <BannerContainer />
        </React.Fragment>
    )
}

export default HomePage
