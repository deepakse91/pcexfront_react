import React, { useState } from 'react';
import * as dispatchToDoList from '../../../redux/disptacher/disptacher';

const AddToDoList = () => {
    const [state, setState] = useState({
        todo_list: '',
    })

    const addNewItem = () => {
        dispatchToDoList.dispatchToDoList(state.todo_list, 'ADD_List');
    }

    return (
        <form>
            <input placeholder="Enter list" onChange={(event) => setState({ todo_list: event.target.value })} />
            <button type="button" className="btn btn-sm btn-info" onClick={() => addNewItem()}>Add</button>
        </form>
    )
}

export default AddToDoList
