import React from 'react';
import { connect } from 'react-redux';
import * as dispatchToDoList from '../../../redux/disptacher/disptacher';

const ShowToDoList = (props) => {
    const delete_List = (index) => {
        dispatchToDoList.dispatchToDoList(index, 'DELETE')
    }

    const edit_List = (item, index) => {

    }

    return (
        <div className="mt-2 p-2" style={{ backgroundColor: 'rgb(33, 67, 165)', color: '#fff' }}>
            <div className="mt-3" style={{ fontWeight: '600' }}>TO DO LIST</div>
            {
                props.list_item.map((item, position) => {
                    return (
                        <div className="ml-3" key={position} style={{ cursor: 'pointer' }}>
                            {item} <a onClick={() => edit_List(item, position)}><i className="fa fa-edit ml-5"></i></a> <a onClick={() => delete_List(position)}><i className="fa fa-trash ml-5"></i></a>
                        </div>
                    )
                })
            }
        </div>
    )
}

const connectComponentToStore = (data) => {
    return {
        list_item: data
    }
}

const ReduxToDoList = connect(connectComponentToStore)(ShowToDoList);
export default ReduxToDoList;
