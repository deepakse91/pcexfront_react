import React from 'react';
import AddToDoList from '../../components/todolist/add';
import ShowToDoList from '../../components/todolist/show';

function demoComponent() {
    return (
        <div className="container mt-3 mb-3">
            <AddToDoList />
            <ShowToDoList />
        </div>
    )
}
;
export default demoComponent
