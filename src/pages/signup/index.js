import React from 'react'

function SignUp(props) {

    const getUrlId = () => {
        console.log(props.match.params.name);
    }

    return (
        <div>
            <h1>This is is sign up component.</h1>
            <button type="button" className="btn btn-info" onClick={() => getUrlId(props.name)}>
                Get URL ID
            </button>
        </div >
    )
}

export default SignUp
