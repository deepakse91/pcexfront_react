import store from '../store/store';
import { getToDoList } from '../actions/actions';

const dispatchToDoList = (list, type) => {
    store.dispatch(getToDoList(list, type))
}
export {
    dispatchToDoList
}