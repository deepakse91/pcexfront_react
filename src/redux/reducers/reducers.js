const initialState = [];

export const ToDoListReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'ADD_List':
            const Newstore = [...state];
            if (Newstore.indexOf(action.payload) === -1) {
                Newstore.push(action.payload);
            } else {
                alert("The item is already in your list");
            }
            return Newstore;
        case 'DELETE':
            const removeFromStore = [...state];
            removeFromStore.splice(action.payload, 1);
            return removeFromStore;
        case 'UPDATE':
            return state;
        default:
            return state;
    }
}