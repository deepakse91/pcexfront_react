import { createStore } from "redux";
import { ToDoListReducer } from "../reducers/reducers";

const store = createStore(ToDoListReducer);

export default store;